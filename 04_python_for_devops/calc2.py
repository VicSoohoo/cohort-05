#!/usr/bin/env python3
import sys

def calculate(num1, num2, op):
    if op == '+':
        return num1 + num2
    elif op == '-':
        return num1 - num2   
    elif op == '*':
        return num1 * num2  
    elif op == '%':
        return num1 % num2   
    else:
        return num1 / num2
    return

args = sys.argv
#print('Arguments are: ', args)
result =  calculate(int(args[1]), int(args[2]), args[3])
print('Result is:', result)
