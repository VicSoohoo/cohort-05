#!/usr/bin/bash

cd $HOME
echo "Current working directory is `pwd`."
echo "============="
echo "full directory listing"
echo "============="
ls -l | grep -v ^t
echo "============="
echo "just list directories"
echo "============="
ls -l | grep ^d
echo "============="
echo "just list files"
echo "============="
ls -l | grep -v ^d | grep -v ^t
echo "============="
echo "list by filesize"
echo "============="
ls -l | sort -k 6 -n | grep -v ^t
