#!/bin/bash

# Convert contents of file into a single line using \n separator
# Author: Victor Soohoo

# Check for file input argument
if [[ ! $1 ]]; then
	echo " "
        echo "Please add your file on the command line!"
	echo " "
        exit 1
fi

# Get the filename
filename="$1"

# Check if file exists
if [[ ! -f  ${filename} ]]; then
	echo " "
        echo "Your ${filename} file doesn't exist.  Please add your file on the command line!"
	echo " "
        exit 1
fi

# file with \n on each line
filecon="${filename}.convert"

# file with \n removed from last line
filerev="${filename}.revised"

# file with single entry
filesin="${filename}.single"

# Add \n to all lines in the file
sed 's/$/\\n/g' ${filename} > ${filecon}

# Remove \n from the last line
sed '$s/..$//' ${filecon} > ${filerev}

# Put all contents of file into a single line
cat ${filerev} | tr -d '\n' > ${filesin}

echo " "
echo "File ${filesin} with single line is created!"
echo "Use [vi ${filesin}] command to view and copy the single line!"
echo " "

#rm ${filecon} ${filerev}

exit 0
