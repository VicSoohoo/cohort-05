#!/bin/bash

# for loop routine to check files
# Author: Victor Soohoo

# Check for file input arguments
if [[ $# -lt 2 ]]; then
	echo " "
        echo "Please add 2 or more files on the command line separated by a space!"
	echo " "
        exit 1
fi

# empty files directory
emptydir="$HOME/VIC/empty/"

# processed files directory
procdir="$HOME/VIC/processed/"

# Get the filename
for filename in "$@"; do
# Check if file exists and not empty
	if [[ -f  ${filename} ]]; then
            if [[ -s ${filename} ]]; then
	        echo "inspected by $USER!" >> ${filename}
                mv ${filename} ${procdir}
                echo "File ${filename} has been inspected and moved to ${procdir} directory!"
	    else 
    	        mv ${filename} ${emptydir}		    
                echo "File ${filename} is empty and moved to ${emptydir} directory!"
            fi
	else		
            echo "Your ${filename} file doesn't exist.  Please check your filename on the command line!"
	fi
done	

exit 0
