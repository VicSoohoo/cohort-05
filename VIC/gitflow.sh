#!/bin/bash

# git workflow to update the repository 
# Author: Victor Soohoo

echo "Execute the $0 script at the top level directory of your repository to include all changes!"
# create a new branch
# ask user for input
read -p "Please input branch name: " branch

if [[ -z ${branch} ]]; then
    echo "No branch name entered!"	
    exit 1
else
    echo "================================================="	
    echo "Creating and checkout ${branch} branch!"	
    echo "================================================="
    git branch ${branch}
    git checkout ${branch}
fi

# stage the file(s) for commit to your local repository
echo "==================="
echo "Running git status!"
echo "==================="
git status
echo "====================================================================="
echo "Adding the file(s) to your local repository and stages it for commit!"
echo "====================================================================="
git add .
echo "==================="
echo "Running git status!"
echo "==================="
git status
echo " "

# commit the file(s) that you've staged in your local repository
read -p "Please input commit message: " commsg

if [[ -z ${commsg} ]]; then
    echo "======================================================================"	
    echo "No commit message entered so just entering NO COMMIT MESSAGE SUPPLIED!"
    echo "======================================================================"
    git commit -m "NO COMMIT MESSAGE SUPPLIED!"
else
    echo "======================================================================"	
    echo "Commiting the file(s) with this message ${commsg}"
    echo "======================================================================"
    git commit -m "${commsg}" 
fi

# push the changes in your local repository to the remote repository
echo "========================================="
echo "Pushing changes to the remote repository!"
echo "========================================="
git push origin ${branch}

# create pull request and merge into master
echo "==================================================================================================================================="
echo "After push is complete go to Branches under your repository in Bitbucket and create a pull request and merge to your master branch!"
echo "==================================================================================================================================="

# update master branch in local repository and cleanup branch after merge
echo "==================================================================================================================="
echo "Run this command [git checkout master] to switch back to the master branch!"
echo "Run this command [git pull origin master] to get the latest code from the remote repository!"
echo "Run this command [git branch -d ${branch}] after the merge to master to delete the ${branch} branch!"
echo "==================================================================================================================="

exit 0
