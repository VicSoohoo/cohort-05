

def fizzbuzz(num):
    result = num
    if num % 15 == 0:
        result = 'fizzbuzz'
    elif num % 3 == 0:
        result = 'fizz'
    elif num % 5 == 0:
        result = 'buzz'
    return result
